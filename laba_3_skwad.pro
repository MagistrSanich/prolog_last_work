/*****************************************************************************

		Copyright (c) My Company

 Project:  LABA_3_SKWAD
 FileName: LABA_3_SKWAD.PRO
 Purpose: No description
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "laba_3_skwad.inc"

domains
	list=symbol*
	listInt=integer*

%database - graph
%	graph(symbol, symbol, integer).
	
predicates
	member(symbol, list).
	max_el(listInt, integer).
	length_1(list, integer, integer).
	start.
	edge(symbol, symbol, integer).
	rebro(symbol, symbol, integer).
	all_paths(symbol, %A [in]
	 symbol, %B [in]
	 list, %[Start] list for last edges [In}
	 list, %list edges [out]
	 integer, %=0 accumulation [in]
	 integer). % sum distance from start to stop [out]
	path(symbol, symbol, list, integer).
	%path1(symbol, symbol, symbol, symbol).
	reverse(list, list, list).
	append(list, list, list).
	all_tops(symbol, list, list).
	get_distances(list, % all combination left [in]
    		list, % all combination right [in]
			integer,% max1 = 0 [in]
			integer) % max2 [out]	
	max_distance(list, list, integer).
	make_pair(list, list, list).
	get_nodes(list, list, integer, symbol, symbol).
	angelina(list, symbol, symbol, integer).

	%all_perm(list, list, list).
	
	%call
	%reverse([a,b,c],X,[]).

clauses
	%edge(a, b, 60). edge(a, c, 15). edge(a, d, 70). edge(b, c, 50).
	edge(b,c,10). edge(a,c,20). edge(b,e,30). edge(b,d,40). edge(a,e,50). edge(e,d,60).
	start :- write("Hello"), nl.
	rebro(X, Y, Length) :- edge(X, Y, Length); edge(Y, X, Length).
	all_paths(B, B, Path, Path, Length, Length).
	all_paths(A, B, Stack, Path, Accum, Length) :-
		rebro(A, A1, Distance),
		not(member(A1, Stack)),
		Accum1 = Accum + Distance,
		append([A1], Stack, Stack1), 
		all_paths(A1, B, Stack1, Path, Accum1, Length).
	path(A, B, Path, Length) :- 
		all_paths(A, B, [A], Path1, 0, Length),
		reverse(Path1, Path, []).
	all_tops(_, Tops, Tops).
	all_tops(A, Stack, Tops):-
		rebro(A, A1, _),
		not(member(A1, Stack)),
		append(Stack, [A1], Stack1),
		all_tops(A1,Stack1, Tops).

	get_distances([],[], Max, Max):-!.
	get_distances([A|Left], [B|Right], Max1, Max_out):-
		path(A, B, Path, Length),
		Length>Max1,
		get_distances(Left, Right, Length, Max_out);
		get_distances(Left, Right, Max1, Max_out).
	max_distance(Left, Right, Max):-findall(Answer, get_distances(Left, Right, 0, Answer), Answers),
		max_el(Answers, Max).
	make_pair(Inp, Left, Right):-length_1(Inp, 0, Len), Len=4, 
		Left=["a","a","a","b","b","c"],
		Right=["b","c","d","c","d","d"];
		length_1(Inp, 0, Len),
		Len=5,
		Left=["a","a","a","a","b","b","b","c","c","d"],
		Right=["b","c","d","e","c","d","e","d","e","e"].
	get_nodes([A|Left], [B|Right], Max, Node_1, Node_2):-
		path(A, B, Path, Length),
		Length=Max, Node_1=A, Node_2=B, !.
	get_nodes([A|Left], [B|Right], Max, Node_1, Node_2):-
		get_nodes(Left, Right, Max, Node_1, Node_2).
	angelina(All_Nodes, Node_1, Node_2, Max):-
		make_pair(All_Nodes, Left, Right),
		max_distance(Left, Right, Max),
		get_nodes(Left, Right, Max, Node_1, Node_2), !.


	%all_perm(X, _, X).
	%all_perm(X, L, Z) :- select(H, L, R), all_perm([H|X], R, Z).


	
	
	member(Elem, [Elem|_Tail]). 	
	member(Elem, [_Head| Tail]):- member(Elem, Tail). 
	reverse([],Z,Z).
	reverse([H|T],Z,Acc) :- reverse(T,Z,[H|Acc]).
	append( [], X, X).                                   % your 2nd line
	append( [X | Y], Z, [X | W]) :- append( Y, Z, W).    % your first line
	max_el([X],X).
	max_el([H|T],R) :- max_el(T,R), H<=R.
	max_el([H|T],H) :- max_el(T,R1), H>R1.
	length_1([], L, L).
	length_1([H|T], L, Out) :- Temp=L+1, length_1(T,Temp, Out).

goal
	%get_distances(["a", "b", "c"], ["c","d","a"], 0, Max), writef("max = %", Max).
	%max_distance(["a", "b", "c"], ["c","d","a"], Max), writef("Max element = %", Max).
	%make_pair(["a", "b", "c", "d"], Left, Right).

	angelina(["a", "b", "c", "d"], Node_1, Node_2, Max).


	%length_1(["a", "b", "c", "d"],0,Length).
	%all_perm([], [a,b,c], X).
	%start, edge(X, _, _), next(a, b).
	%all_paths(b, d, ["b"], Path, 0, Length).
	%path(b,d,Path,Length).
	%all_tops(a, ["a"], Tops).
	%Start="g", append([Start], ["a", "b"], List).
  
