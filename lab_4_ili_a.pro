/*****************************************************************************

		Copyright (c) My Company

 Project:  LAB_4_ILI_A
 FileName: LAB_4_ILI_A.PRO
 Purpose: Sdat dolpany laby
 Written by: Visual Prolog
 Comments:
******************************************************************************/

include "lab_4_ili_a.inc"

domains
	listStr=symbol*
	listInt=integer*

database - flowers
	flowers(symbol,symbol,integer,symbol).

predicates
	connectionFacts.
	start.
	next(symbol).
	findFactsByColors(symbol).
	findFactsByYears(symbol).
	nextStep(listStr,listInt).
	writeList(listStr,listInt).
	insert_sort(listInt,listInt,listStr,listStr).
	insert(integer,listInt,listInt,symbol,listStr,listStr).

clauses
	connectionFacts:-consult("flowers.txt",flowers).
	start:- write("Enter 1 or 2"),nl,readln(X),next(X).
	next("1"):-nl,write("Enter color flowers"), nl,readln(X), findFactsByColors(X).
	next("2"):- nl,write("Enter life span"),nl, readln(X), findFactsByYears(X).
	findFactsByColors(X):-findall(H,flowers(H,X,_,_),L),nextStep(L,L1),insert_sort(L1,L2,L,L3),writeList(L3,L2).
	findFactsByYears(X):-findall(H,flowers(H,_,_,X),L),nextStep(L,L1),insert_sort(L1,L2,L,L3), writeList(L3,L2).
	nextStep([],[]).
	nextStep([H|T],L):-flowers(H,_,Z,_),nextStep(T,L1),L=[Z|L1].
	writeList([],[]).
	writeList([Name|T],[Height|T1]):-write("Name = " ,Name),write(", Height = " ,Height),nl, writeList(T,T1).
	
	insert_sort([],[],[],[]).
	insert_sort([X|L1],S1,[Y|L2],S2):- insert_sort(L1,S3,L2,S4),insert(X,S3,S1,Y,S4,S2).
	insert(X,[A|S1],[A|S2],Y,[A1|S3],[A1|S4]):-A>X,!,insert(X,S1,S2,Y,S3,S4).
	insert(X,S1,[X|S1],Y,S2,[Y|S2]).

goal
	connectionFacts,start.
